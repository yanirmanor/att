import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthenticationService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    returnUrl: string;
    error: boolean;

    constructor(  private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService,){}
    
    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
        this.authenticationService.fetchAllUsers();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        this.error = false;
        this.authenticationService.login(this.model.username, this.model.company)
            .then(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.error = true;
                });
    }
}