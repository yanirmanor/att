import { Component, OnInit } from '@angular/core';

import { User, Albums } from '../_models/index';
import { AlbumsService } from '../_services/index';
import * as _ from "lodash";

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    albums: any = [];

    constructor(private albumsService: AlbumsService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAlbums();
    }

    private loadAlbums() {
        this.albumsService.getAlbumsAndImages().subscribe(albums => { 
            let data = _.merge(albums[0],albums[1]);
            data.map((item)=>{
                let temp = new Albums();
                temp.title = item.title;
                temp.image = item.thumbnailUrl;
                this.albums.push(temp);
            })
        })
    }
}