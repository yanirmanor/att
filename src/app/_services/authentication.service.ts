import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import * as _ from "lodash";
import { User } from '../_models/index';


@Injectable()
export class AuthenticationService {
    public users: User[] = [];

    constructor(private http: Http) { }

    fetchAllUsers() {
        let that = this;
        return this.http.get('http://jsonplaceholder.typicode.com/users')
            .retry(2)
            .map((response: Response) => response.json())
            .catch((error: any) => Observable.throw(error || 'Server error'))
            .subscribe((data) => {
                console.log(data);
                data.map((user)=>{
                    that.users.push({
                        username: user.username,
                        company: user.company.name
                    })
                })
            });

    }

    login(username: string, company: string) {
        return new Promise((resolve, reject) => {
            let loginUser = {
                username: username,
                company: company
            };
            let findUser = _.find(this.users, loginUser);
            if (findUser) {
                localStorage.setItem('currentUser', JSON.stringify(loginUser));
                return resolve(true);
            } else {
                return reject();
            }
        });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}