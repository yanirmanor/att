import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AlbumsService {
    constructor(private http: Http) { }

    getAlbumsAndImages(){
        return Observable.forkJoin(
                    this.http.get('https://jsonplaceholder.typicode.com/albums').map((response : Response) => response.json()),
                    this.http.get('https://jsonplaceholder.typicode.com/photos').map((response : Response) => response.json())
            );
    }
};