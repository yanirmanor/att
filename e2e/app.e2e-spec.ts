import { AttPage } from './app.po';

describe('att App', () => {
  let page: AttPage;

  beforeEach(() => {
    page = new AttPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
